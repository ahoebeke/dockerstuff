#!/bin/bash

if
  [ -z ${SYSINIT+x} ];
then
  doinit=true ;
  echo ' ';
  echo '#########################################';
  echo '#########################################';
  echo '######                             ######';
  echo '######    launch INIT scripts ?    ######';
  echo '######                             ######';
  echo '#########################################';
  echo '#########################################';
  echo '##';
else
  echo ' ';
  echo 'System has been initialised previously. Enjoy your session.';
  echo ' ';
  doinit=false;
fi

while $doinit; do
    read -p "## Docker host LAN IP address?  " myip
    read -p "## Number for the Drupal DB name?  " mynum
    read -p "## Do you wish to initialise the system?  " yn
    case $yn in
        [Yy]* ) SYSINIT="SystemInitialised"; export SYSINIT; echo 'SYSINIT="SystemInitialised"; export SYSINIT;' | cat - ~/.bashrc > /tmp/out.bashrc && mv /tmp/out.bashrc ~/.bashrc ; cat ~/.bashrc ;  echo "Initialising system"; echo ' '; bash /init/init.sh $myip $mynum ; break;;
        [Nn]* ) break;;
        * ) echo "Please answer Yes or No.";;
    esac
done


