FROM php:5.5-apache
MAINTAINER Andy Hoebeke <ahoebeke@gmail.com>

RUN a2enmod rewrite

# install the PHP extensions we need
RUN apt-get update \ 
 && apt-get install -y \ 
        libpng12-dev \
	libjpeg-dev \ 
	libpq-dev \
        mysql-client \
	drush \ 
	nano \
        sudo \
 && rm -rf /var/lib/apt/lists/* \
 && apt-get clean \
 && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
    # removed pdo from original Dockerfile, since its compiled into php and gives an annoying warning otherwise
 && docker-php-ext-install gd mbstring pdo_mysql pdo_pgsql
	

WORKDIR /var/www/html

# https://www.drupal.org/drupal-7.38-release-notes or for all: https://www.drupal.org/node/3060/release
ENV DRUPAL_VERSION 7.38
ENV DRUPAL_MD5 c18298c1a5aed32ddbdac605fdef7fce
# to make sure nano editor works
ENV TERM vt100  

COPY ./onlogin.sh /init/
COPY ./init.sh /init/
COPY ./init1.sh /init/
COPY ./init2.sh /init/

RUN echo 'source /init/onlogin.sh' >> ~/.bashrc
RUN curl -fSL "http://ftp.drupal.org/files/projects/drupal-${DRUPAL_VERSION}.tar.gz" -o drupal.tar.gz \
	&& echo "${DRUPAL_MD5} *drupal.tar.gz" | md5sum -c - \
	&& tar -xz --strip-components=1 -f drupal.tar.gz \
	&& rm drupal.tar.gz \
	&& chown -R www-data:www-data sites \
	&& echo "ini_set('memory_limit','200M');" >> ./sites/default/default.settings.php \
        && echo 'post_max_size = 100M' >> /etc/php5/cli/php.ini \
        && echo 'upload_max_filesize = 20M' >> /etc/php5/cli/php.ini \
        && mkdir -p /init/ \
        && chmod u+x /init/init.sh \
        && chmod u+x /init/init2.sh

# Do this in a separate image layer, in case I adapt the values below
#RUN bash /init/init.sh 192.168.200.201 6 \
#        && bash /init/init1.sh && bash /init/init2.sh

EXPOSE 80

ENTRYPOINT ["/usr/sbin/apache2ctl","-D FOREGROUND"]

