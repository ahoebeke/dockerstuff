#!/bin/bash

# launching the site install... Database should be empty or not exist! Also, there should be a parameter, being the publicly accessible LAN IP address of the Docker host
# param 1 : LAN IP of docker host
# param 2 : database name suffix for Drupal database

drush site-install -y standard  --db-url=mysql://drupaladmin:'letmein'@$1/drupal$2  --account-name=drupaladmin  --account-pass=letmein  --site-name=DockerDrupal

# setting up folder and file permissions for the Drupal installation 
# sets up the user owning the installation as well
# configures group for www-data user
bash /init/init1.sh

# installs and enables the needed modules
# disables some modules
bash /init/init2.sh
