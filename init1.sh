#!/bin/bash
# to be executed after installing the Drupal site
#
# Script should be run with sudo, to enable user creation if necessary.
# Most other commands will be run with sudo -u $USERNAME <command> to avoid problems with ownership and group settings.

### normally not necessary, if executed by vagrant:vagrant-www user

USERNAME=drupaluser
PASSWORD=drupalpass
DRUPALGROUP='drupal-www'

getent passwd $USERNAME || ( useradd $USERNAME && echo "$USERNAME:$PASSWORD" | chpasswd )

##can also use id -u USERNAME &>/dev/null || ...

groupadd $DRUPALGROUP -f 
usermod -a -G $DRUPALGROUP 'www-data'

###

sudo -u $USERNAME mkdir -p ./sites/default/private-files

chown -R $USERNAME ./
chgrp -R $DRUPALGROUP ./

###

# User has all access rights. Group can only read and traverse. Other no access.
sudo -u $USERNAME chmod -R 750 ./

# User and group should be able to write to the files/ folder.
sudo -u $USERNAME chmod -R ug+rw ./sites/default/files
sudo -u $USERNAME chmod -R ug+rw ./sites/default/private-files

# Group bit is set, to mark they belong to the vagrant-www group.
sudo -u $USERNAME chmod g+s ./sites/default/files
sudo -u $USERNAME chmod g+s ./sites/default/private-files

# this one should not be writable
sudo -u $USERNAME chmod 444 ./sites/default/settings.php

###

